using System;

namespace Task4
{
	class Program
    {
		public static void Main()
        {
            // Створення об'єктів.
			int n = 4;
            var matrix1 = new Matrix(n, n);
            var matrix2 = new Matrix(n, n);
			Matrix result;

            // Заповнення випадковими числами.
            Matrix.FillRnd(matrix1);
            Matrix.FillRnd(matrix2);

            // Вивід матриць з якими проводитимемо операції
            Console.WriteLine("1st matrix:\n{0}", matrix1);
            Console.WriteLine("2nd matrix:\n{0}", matrix2);

            // Додавання матриць.
            Console.WriteLine("Summ 1st & 2nd matrix:");
            result = matrix1 + matrix2;
            Console.WriteLine(result);

            // Віднімання матриць.
            Console.WriteLine("Substract 1st & 2nd matrix:");
            result = matrix1 - matrix2;
            Console.WriteLine(result);
            
            // Множення матриць.
            Console.WriteLine("Mult 1st & 2nd matrix:");
            result = matrix1 * matrix2;
            Console.WriteLine(result.ToString());
            
            // Множення матриці на число.
			double num = 5;
            Console.WriteLine("Mult scalar {0} in 1st matrix:", num);
            result = matrix1 * num;
            Console.WriteLine(result);

            // Транспонована матриця.
            Console.WriteLine("Transpose 1st matrix:");
            result = Matrix.Transpose(matrix1);
            Console.WriteLine(result);
            
            // Отримання підматриці.
            Console.WriteLine("Submatrix of 1st matrix:");
            result = Matrix.GetSubMatrix(matrix1, 1, 1, 2, 2);
            Console.WriteLine(result);
 
            Console.WriteLine("1st matrix = 2nd? - {0}", matrix1 == matrix2);
        }
    }
	
	public class Matrix
    {
        #region Fields
        private double[,] _matrix;
		private static Random rNumber = new Random();
        #endregion

        #region Constructors
        public Matrix(int rows, int cols)
        {
            if (rows <= 0 || cols <= 0)
                throw new ArgumentOutOfRangeException();

            this.Rows = rows;
            this.Cols = cols;
            _matrix = new double[Rows, Cols];
        }
        #endregion

        #region Properties
        public int Rows { get; private set; }
        public int Cols { get; private set; }
        #endregion
        
        #region Indexer
        public double this[int rows, int cols]
        {
            get
            {
                if ((rows < 0 || rows >= this.Rows) || (cols < 0 || cols >= this.Cols))
                   throw new ArgumentOutOfRangeException();
                return _matrix[rows, cols];
            }

            set
            {
                if ((rows < 0 || rows >= this.Rows) || (cols < 0 || cols >= this.Cols))
                   throw new ArgumentOutOfRangeException();
                _matrix[rows, cols] = value;
            }
        }
        #endregion

        #region Methods
        // Add a 2 matrix.
        public static Matrix Add(Matrix matrix1, Matrix matrix2)
        {
            if (matrix1 == null || matrix2 == null)
                throw new ArgumentNullException();
            
            if (matrix1.Rows != matrix2.Rows || matrix1.Cols != matrix2.Cols)
            {
                throw new ArgumentException("Error! Matrixs must be equal.");
            }

            Matrix buf = new Matrix(matrix1.Rows, matrix1.Cols);
            for (var i = 0; i < buf.Rows; i++)
                for (var j = 0; j < buf.Cols; j++)
                    buf[i, j] = matrix1[i, j] + matrix2[i, j];

            return buf;
        }

		// Substract a 2 matrix.
		public static Matrix Substract(Matrix matrix1, Matrix matrix2)
        {
            if (matrix1 == null || matrix2 == null)
                throw new ArgumentNullException();
            
            if (matrix1.Rows != matrix2.Rows || matrix1.Cols != matrix2.Cols)
            {
                throw new ArgumentException("Error! Matrixs must be equal.");
            }

            Matrix temp = new Matrix(matrix1.Rows, matrix1.Cols);
            
			for (var i = 0; i < temp.Rows; i++)
                for (var j = 0; j < temp.Cols; j++)
                    temp[i, j] = matrix1[i, j] - matrix2[i, j];

            return temp;
        }

        // Mult matrix by scalar.
        public static Matrix Multiply(Matrix matrix, double value)
        {
            Matrix temp = new Matrix(matrix.Rows, matrix.Cols);
           
  		    for (var i = 0; i < temp.Rows; i++)
                for (var j = 0; j < temp.Cols; j++)
                    temp[i, j] = matrix[i, j] * value;

            return temp;
        }
        
        // Mult of 2 matrix.
        public static Matrix Multiply(Matrix matrix1, Matrix matrix2)
        {
            if (matrix1.Rows != matrix2.Cols)
            {
                throw new ArgumentException("Wrong dimensions of matrix!");
            }

            Matrix temp = new Matrix(matrix1.Rows, matrix2.Cols);
            for (var i = 0; i < temp.Rows; i++)
                for (var j = 0; j < temp.Cols; j++)
                    for (var k = 0; k < matrix1.Cols; k++)
                        temp[i, j] = matrix1[i, k] * matrix2[k, j];
           
   		    return temp;
        }

        public Matrix Multiply(Matrix matrix)
        {
            if (this.Rows != matrix.Cols)
            {
                throw new ArgumentException("Wrong dimensions of matrix!");
            }
           
            return Matrix.Multiply(this, matrix);
        }
        
        // Get subMatrix.
        public static Matrix GetSubMatrix(Matrix matrix, int startRow, int startCol, int endRow, int endCol)
        {
            int rowsRange = endRow - startRow;
            int colsRange = endCol - startCol;
			
			if (rowsRange < 1 || colsRange < 1 || rowsRange <= 0 || colsRange <= 0 
                || startRow < 0 || startCol < 0 || startRow >= matrix.Rows || startCol >= matrix.Cols
                || endRow < 0 || endCol < 0 || endRow >= matrix.Rows || endCol >= matrix.Cols)
                throw new ArgumentOutOfRangeException();

            var buf = new Matrix(endRow - startRow + 1, endCol - startCol + 1);
            int it = 0;
            
            for (var i = startRow; i <= endRow; i++)
            {
                int jt = 0;
                for (var j = startCol; j <= endCol; j++)
                {
                    buf[it, jt] = matrix[i, j];
                    jt++;
                }
                it++;
            }
            
            return buf;
        }
        
        // Transpose matrix.
        public static Matrix Transpose(Matrix matrix)
        {
            Matrix buf = new Matrix(matrix.Cols, matrix.Rows);
            for (int i = 0; i < matrix.Rows; i++)
                for (int j = 0; j < matrix.Cols; j++)
                    buf[j, i] = matrix[i, j];
            
            return buf;
        }
        
		//Fill matrix rnd number
		public static void FillRnd(Matrix matrix)
		{
   		    for (int i = 0; i < matrix.Rows; i++)
			{
				for (int j = 0; j < matrix.Cols; j++)
				{
					matrix[i, j] = rNumber.Next(47);
				}
			}
		}
		
        public override string ToString()
        {
            string result = "\n";

            for (var i = 0; i < Rows; i++)
            {
                for (var j = 0; j < Cols; j++)
                {
                    result += String.Format("{0}\t", _matrix[i, j]);
                }
                result += "\n";
            }

            return result;
        }

        public override bool Equals(object obj)
        {
            bool result = false;

			if (obj is Matrix)
			{
                result = Equals((Matrix) obj);
			}

			return result;
        }

        public override int GetHashCode()
        {
             return base.GetHashCode();
        }
        #endregion

        #region Operators
        public static Matrix operator +(Matrix matrix1, Matrix matrix2)
        {
            return Matrix.Add(matrix1, matrix2);
        }

        public static Matrix operator -(Matrix matrix)
        {
            return Matrix.Multiply(matrix, -1);
        }
        public static Matrix operator -(Matrix matrix1, Matrix matrix2)
        {
            return Matrix.Substract(matrix1, matrix2);
        }

        public static Matrix operator *(Matrix matrix1, Matrix matrix2)
        {
            return Matrix.Multiply(matrix1, matrix2);
        }

        public static Matrix operator *(Matrix matrix, double number)
        {
            return Matrix.Multiply(matrix, number);
        }

        public static bool operator ==(Matrix matrix1, Matrix matrix2)
        {
            return Matrix.IsEqual(matrix1, matrix2);
        }

        public static bool operator !=(Matrix matrix1, Matrix matrix2)
        {
            return !Matrix.IsEqual(matrix1, matrix2);
        }
        #endregion
        
        #region Private methods
        private static bool IsEqual(Matrix matrix1, Matrix matrix2)
        {
            bool isEqual = true;
            
            if ((object)matrix1 == null || (object)matrix2 == null || matrix1.Rows != matrix1.Rows || matrix1.Cols != matrix2.Cols)
            {
                isEqual = false;
            }
            else
            {
                for (var i = 0; i < matrix1.Rows; i++)
                {
                    for (var j = 0; j < matrix2.Cols; j++)
                    {
                        if (matrix1[i, j] != matrix2[i, j])
                        {
                            isEqual = false;
                            break;
                        }
                    }
                }
            }

            return isEqual;
        }
        #endregion
    }
}